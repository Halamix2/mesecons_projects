interrupt(0.3, "clock")

if (event.type == "interrupt") then
    port.a = not port.a
    port.b = not port.b
    port.c = not port.c
    port.d = not port.d
end

if event.type == "digiline" then
    print(event.channel)
    print(event.msg)
end