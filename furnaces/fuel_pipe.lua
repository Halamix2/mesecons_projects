--[[PIPE CONTROLLER
version 0.3.2
UNSTABLE, ALPHA ETC. DISCLAIMER
originally programmed by Halamix2, if broken contact me
revisioned by drebrez

Usually it should push fuel to next pipe or input chest in case of last pipe.

Each processed item (detected by the digiline chest) increases demand
except when the time between the first processed item is less than the
coal burning time (40s).
When the processed item arrives after 40s, a new fuel has begin to burn, so
the time must be resetted.
When demand is above 0 then incoming fuel is redireted to the furnace
and demand is lowered by 1, and it continues to refill until the demand is 0.

LIMIATIONS: 
* for now it only works with coal
    it would be very hard to mix fuels in automated way
    furnace can only have one type of fuel inside at a time
* works with one furnace
    not usable with sapling > glue > fiber machine yet

MAKE SURE TO HAVE AT LEAST 10 COALS IN EACH FURNACE
AS TIME BETWEEN SMELTING, CHEST RECEIVING ITEM AND NEAREST INCOMING COAL MAY BE LONG
]]--

--this section should contain most of the vars different for each pipe
if (event.type == "program") then
    --set demand to 0
    mem.demand = 0

    --set item that this furnace delivers
    mem.item = "default:steel_ingot"
    
    --pipes
    --furnace
    mem.furnace = "green"
    --next
    mem.forward = "blue"

    --time of last item arrival in chest
    mem.time = 0
    
elseif (event.type == "item") then

    --is fuel needed in this furnace?
    if(mem.demand >= 1) then
        if (event.itemstring == "default:coal_lump") then
            mem.demand = mem.demand - 1
            --and send it to furnace
            return mem.furnace
        end
    end

    --send everything else forward
    return mem.forward
    
--proess chest communication
elseif( event.type == "digiline" and event.channel == "buffer") then
    --was it required item?
    if(event.msg == "put "..mem.item) then
        --it's been over 40s since the first item?
        if( os.time() - mem.time > 40 ) then
            --fire surely stopped burning and new one started, request one more
            mem.demand = mem.demand + 1
            --reset the time
            mem.time = os.time()
        end
    end
end