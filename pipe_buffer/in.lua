--old code, used pipe loopback

--this section should contain most of the vars different for each pipe
if (event.type == "program") then
    mem.inPipe = 0
    mem.capacity = 4
    
elseif (event.type == "item") then
    -- for shorter lines
    local itemName = event.itemstring
    
    --is fuel needed in this furnace?
    if(mem.inPipe < mem.capacity) then
        mem.inPipe = mem.inPipe + 1
        return "red"
    else
        --send everything else forward
        return "black"
    end
elseif( event.type == "digiline") then
    --second chest, for now only one digilined
    if (event.channel == "recv") then
        mem.inPipe = mem.inPipe - 1
    end
end